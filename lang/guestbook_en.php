﻿<?php
    /**
      @file   lang/en.php
      @author Ruhland Matthias

      @brief  The english language file

      @changes
    */

    $lang = array(
    'lang'                      => 'en',

    /************************************************************************************/
    // Guestbook
    /************************************************************************************/

    // Header
    'head_link_start'           => 'Start',
    'head_link_guestbook'       => 'Guestbook',

    // Footer
    'footer_impressum'          => 'Impressum',
    'footer_copyright_1'        => 'Copyright &copy;',
    'footer_copyright_2'        => 'JaRuhl',

    // Mainpage
    'start_title'               => 'Demonstrationpage for the guestbook',
    'start_keywords'            => 'php, guestbook',
    'start_content'             => 'This is a simple demonstrationpage for my simple guestbook',
    'start_mainpage'            => 'This could be another page.',

    // Guestbook
    'guest_title'               => 'JaRuhl\'s Guestbook',
    'guest_content'             => 'The guestbook of JaRuhl',
    'guest_keywords'            => 'guestbook, jaruhl',
    'guestbook_title'           => 'Guestbook of JaRuhl',
    'guest_legend'              => 'New entry',
    'guest_name'                => 'Name',
    'guest_name_data'           => '* shouldn\'t be empty, max. 30 signs',
    'guest_mail'                => 'E-Mail',
    'guest_mail_data'           => 'max. 30 signs',
    'guest_message'             => 'message',
    'guest_send'                => 'send',
    'guest_clear'               => 'delete',
    'guest_message_sent'        => 'The Message was sent, but the administration must check it perhaps to show it.',
    'guest_message_send_er'     => 'An error occured. The message couldn\'t be sent.',
    'guestbook_page'            => 'page',
    'guest_mail_subject'        => 'New Entry!',
    'guest_mail_body'           => 'There is a new entry in your guestbook',
    'guest_mail_from'           => 'From',
    'guest_mail_mail'           => 'E-Mail',
    'guest_mail_date'           => 'Date',
    'guest_mail_ip'             => 'IP',
    'guest_mail_message'        => 'Message',
    'guest_mail_link'           => 'Administrate the new entry',
    'guest_mail_error'          => 'Couldn\'t send the mail.',
    'guest_mail_ok'             => 'A mail was sent to the administrator.',

    /************************************************************************************/
    // Administration
    /************************************************************************************/
    'guest_login_title'         => 'Guestbook - Administration',
    'guest_user'                => 'User',
    'guest_login'               => 'Login',
    'guest_login_ok'            => 'You are now logged in.<br />
                                    Please refresh the page or click ',
    'guest_login_here'          => 'here.',
    'guest_login_err'           => 'A Error occurred during login.<br />
                                    Wrong password?',
    'guest_logout'              => 'logout',
    'guest_logged_out'          => 'You are now logged out.<br />
                                    Please refresh the page or click',
    'guest_entries'             => 'Entries',
    'guest_passwd_chk'          => 'Change password',
    'guest_users_admin'         => 'Administrate Users',
    'guest_entry_deleted'       => 'Entry deleted',
    'guest_entry_deleted_err'   => 'Could\'t delete the entry',
    'guest_show_entry'          => 'Entry will be shown',
    'guest_show_entry_err'      => 'Can\'t show the Entry',
    'guest_hide_entry'          => 'Entry will be hidden',
    'guest_hide_entry_err'      => 'Can\'t hide the Entry',
    'guest_show'                => 'show',
    'guest_hide'                => 'hide',
    'guest_delte_entry'         => 'delete',
    'guest_user_not_valid'      => 'Access permitted!',
    'guest_user_not_loggedin'   => 'You are not logged in!',
    'guest_fill_all'            => 'Not all fields are filled!',
    'guest_pass_old'            => 'Old password',
    'guest_pass_new'            => 'new password',
    'guest_cancel'              => 'cancel',
    'guest_same_pw'             => 'password is the same!',
    'guest_pw_chked'            => 'password changed',
    'guest_pw_chked_err'        => 'couldn\'t change the password!',
    'guest_create_user'         => 'User created',
    'guest_create'              => 'created',
    'guest_created'             => 'User successfully created',
    'guest_created_err'         => 'User could\'t be created!',
    'guest_userlevel'           => 'Userlevel',
    'guest_userlevel_1'         => 'Administrate entries',
    'guest_userlevel_2'         => 'Administrate entries and users',
    'guest_chk_level'           => 'change userlevel',
    'guest_change'              => 'change',
    'guest_level_chked'         => 'userlevel changed',
    'guest_level_chked_err'     => 'couldn\'t change userlevel!',
    'guest_user_del'            => 'user deleted',
    'guest_user_del_err'        => 'couldn\'t delete user!',
    'guest_user_id'             => 'user-ID',
    'guest_first_login'         => 'Please login!',
    'guest_no_entry'            => 'No Entry available!',

    /************************************************************************************/
    // Installation
    /************************************************************************************/
    'guest_install_title'       => 'Guestbook - Installation',
    'guest_install_headline'    => 'JR\'s Guestbook - Installation',
    'guest_install_attention'   => 'Please check all settings.<br />
                                    Or else errors can ocure during installation!<br />
                                    The settings are stored in settings/settings.php.',
    'guest_install_dbs'         => 'Database - Settings',
    'guest_host'                => 'Host',
    'guest_user'                => 'Username',
    'guest_pass'                => 'Password',
    'guest_db_exist'            => 'Database exist',
    'guest_yes'                 => 'Yes',
    'guest_no'                  => 'No',
    'guest_mail_check'          => 'E-Mail-Notification',
    'guest_do_install'          => 'install',
    'guest_password_information'    => 'If you want to use a other method to create the password,
            you can check the password.inc.php.<br />
            In the function password_create you can insert every method your PHP-installation
            has.',
    'guest_conn_db'             => 'Connection to the database',
    'guest_conn_ok'             => 'etablished',
    'guest_conn_err'            => 'error!',
    'guest_db_create'           => 'Create database',
    'guest_db_create_ok'        => 'done',
    'guest_db_create_err'       => 'error!!!',
    'guest_create_table'        => 'Create table and id',
    'guest_success'             => 'successfully',
    'guest_error'               => 'error!',
    'guest_create_name'         => 'Create field name',
    'guest_create_mail'         => 'Create field e-mail',
    'guest_create_message'      => 'Create field message',
    'guest_create_time'         => 'Create field date',
    'guest_create_ip'           => 'Create field ip-addresses',
    'guest_create_state'        => 'Create field state',
    'guest_install_ok'          => 'All successfully installed',
    'guest_install_err'         => 'error while installing',
    'guest_root_pw'             => 'root-user',
    'guest_user_name'           => 'username',
    'guest_user_name_data'      => '* shouldn\'t be empty, max 30 signs',
    'guest_password'            => 'Password',
    'guest_create_user'         => 'create user',
    'guest_user_created'        => 'user successfully created',
    'guest_delete_install'      => 'You can now delete the installation-folder.',
    'guest_user_create_err'     => 'Couldn\'t create users!'
    );
?>