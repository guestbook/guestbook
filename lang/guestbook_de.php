﻿<?php
    /**
      @file   lang/de.php
      @author Ruhland Matthias

      @brief  The german language file
    */

    $lang = array(
    'lang'                      => 'de',

    /************************************************************************************/
    // Guestbook
    /************************************************************************************/

    // Header
    'head_link_start'           => 'Start',
    'head_link_guestbook'       => 'Gästebuch',

    // Footer
    'footer_impressum'          => 'Impressum',
    'footer_copyright_1'        => 'Copyright &copy;',
    'footer_copyright_2'        => 'JaRuhl',

    // Mainpage
    'start_title'               => 'Demoseite für Gästebuch',
    'start_keywords'            => 'php, gästebuch, guestbook',
    'start_content'             => 'Das ist eine einfache Demoseite für mein einfaches Gästebuch',
    'start_mainpage'            => 'Das könnte jede beliebige Start-Seite sein.',

    // Guestbook
    'guest_title'               => 'JaRuhl\'s Gästebuch',
    'guest_content'             => 'Das Gästebuch von JaRuhl',
    'guest_keywords'            => 'Gästebuch, guestbook, jaruhl',
    'guestbook_title'           => 'Gästebuch von JaRuhl',
    'guest_legend'              => 'Eintragen ins Gästebuch',
    'guest_name'                => 'Name',
    'guest_name_data'           => '* darf nicht leer sein, maximal 30 Zeichen',
    'guest_mail'                => 'E-Mail',
    'guest_mail_data'           => 'maximal 30 Zeichen',
    'guest_message'             => 'Nachricht',
    'guest_send'                => 'Eintragen',
    'guest_clear'               => 'löschen',
    'guest_message_sent'        => 'Nachricht wurde gesendet, der Administrator muss sie eventuell erst noch freischalten.',
    'guest_message_send_er'     => 'Es ist ein Fehler aufgetreten. Die Nachricht wurde nicht übermittelt werden.',
    'guestbook_page'            => 'Seite',
    'guest_mail_subject'        => 'Neuer Gästebucheintrag!',
    'guest_mail_body'           => 'Es ist ein neuer Gästebucheintrag eingegangen',
    'guest_mail_from'           => 'Von',
    'guest_mail_mail'           => 'E-Mail',
    'guest_mail_date'           => 'Datum',
    'guest_mail_ip'             => 'IP',
    'guest_mail_message'        => 'Nachricht',
    'guest_mail_link'           => 'Neuen Eintrag administrieren',
    'guest_mail_error'          => 'Eine E-Mail konnte leider nicht verschickt werden.',
    'guest_mail_ok'             => 'Eine E-Mail wurde an den Administrator erfolgreich verschickt.',

    /************************************************************************************/
    // Administration
    /************************************************************************************/
    'guest_login_title'         => 'Gästebuch - Administration',
    'guest_user'                => 'Benutzer',
    'guest_login'               => 'Einloggen',
    'guest_login_ok'            => 'Sie wurden erfolgreich eingeloggd.<br />
                                    Bitte aktualisieren sie die Seite neu oder klicken',
    'guest_login_here'          => 'hier.',
    'guest_login_err'           => 'Es ein Fehler aufgetreten beim Login.<br />
                                    Falsches Passwort?',
    'guest_logout'              => 'Ausloggen',
    'guest_logged_out'          => 'Sie wurden ausgelogged.<br />
                                    Bitte aktualisieren sie die Seite neu oder klicken',
    'guest_entries'             => 'Einträge',
    'guest_passwd_chk'          => 'Passwort ändern',
    'guest_users_admin'         => 'Benutzer verwalten',
    'guest_entry_deleted'       => 'Eintrag wurde gelöscht',
    'guest_entry_deleted_err'   => 'Eintrag konnte nicht gelöscht werden',
    'guest_show_entry'          => 'Eintrag wird ab sofort angezeigt',
    'guest_show_entry_err'      => 'Eintrag kann nicht angezeigt werden',
    'guest_hide_entry'          => 'Eintrag wird ab sofort nicht mehr angezeigt',
    'guest_hide_entry_err'      => 'Eintrag kann nicht ausgeblendet werden',
    'guest_show'                => 'zeigen',
    'guest_hide'                => 'verbergen',
    'guest_delte_entry'         => 'löschen',
    'guest_user_not_valid'      => 'Ihre Rechte reichen nicht aus!',
    'guest_user_not_loggedin'   => 'Sie sind nicht eingeloggt!',
    'guest_fill_all'            => 'Nicht alle Felder ausgefüllt!',
    'guest_pass_old'            => 'Altes Passwort',
    'guest_pass_new'            => 'Neues Passwort',
    'guest_cancel'              => 'abbrechen',
    'guest_same_pw'             => 'Passwort ist identisch!',
    'guest_pw_chked'            => 'Passwort wurde geändert',
    'guest_pw_chked_err'        => 'Passwort konnte nicht geändert werden!',
    'guest_create_user'         => 'Benutzer erstellen',
    'guest_create'              => 'erstellen',
    'guest_created'             => 'Benuter erfolgreich erstellt',
    'guest_created_err'         => 'Benutzer konnte nicht erstellt werden!',
    'guest_userlevel'           => 'Benutzerlevel',
    'guest_userlevel_1'         => 'Einträge verwalten',
    'guest_userlevel_2'         => 'Einträge und Benutzer verwalten',
    'guest_chk_level'           => 'Userlevel ändern',
    'guest_change'              => 'ändern',
    'guest_level_chked'         => 'Userlevel wurde geändert',
    'guest_level_chked_err'     => 'Userlevel konnte nicht geändert werden!',
    'guest_user_del'            => 'Benutzer wurde gelöscht',
    'guest_user_del_err'        => 'Benutzer konnte nicht gelöscht werden!',
    'guest_user_id'             => 'Benutzer-ID',
    'guest_first_login'         => 'Zum weitern Vorgehen melden sie sich bitte an!',
    'guest_no_entry'            => 'Kein Eintrag vorhanden!',

    /************************************************************************************/
    // Installation
    /************************************************************************************/
    'guest_install_title'       => 'Gästebuch - Installation',
    'guest_install_headline'    => 'JR\'s Gästebuch - Installation',
    'guest_install_attention'   => 'Bitte überprüfen sie alle Einstellungen.<br />
                                    Ansonsten könnte die Installation fehlschlagen!<br />
                                    Die Einstellungen befinden sich in settings/settings.php.',
    'guest_install_dbs'         => 'Datenbank - Einstellungen',
    'guest_host'                => 'Host',
    'guest_user'                => 'Benutzername',
    'guest_pass'                => 'Passwort',
    'guest_db_exist'            => 'Datenbank vorhanden',
    'guest_yes'                 => 'Ja',
    'guest_no'                  => 'Nein',
    'guest_mail_check'          => 'E-Mail-Benachrichtigung',
    'guest_do_install'          => 'installieren',
    'guest_password_information'    => 'Wenn sie eine besonder Methode zur Passwort-Verschlüsselung
            verwenden wollen, werfen sie einen blick in die password.inc.php.<br />
            In der Funktion password_create kann jegliche Methode eingefügt werden, die von der
            jeweiligen PHP-Installation unterstützt wird.',
    'guest_conn_db'             => 'Verbindung zur Datenbank',
    'guest_conn_ok'             => 'hergestellt',
    'guest_conn_err'            => 'Verbindungsfehler!',
    'guest_db_create'           => 'Erstelle Datenbank',
    'guest_db_create_ok'        => 'erstellt',
    'guest_db_create_err'       => 'nicht erstellt!!!',
    'guest_create_table'        => 'Erstelle Tabelle und ID',
    'guest_success'             => 'erfolgreich',
    'guest_error'               => 'Fehler!',
    'guest_create_name'         => 'Erstelle Namens-Feld',
    'guest_create_mail'         => 'Erstelle E-Mail-Feld',
    'guest_create_message'      => 'Erstelle Nachrichten-Feld',
    'guest_create_time'         => 'Erstelle Datums-Feld',
    'guest_create_ip'           => 'Erstelle IP-Addressen-Feld',
    'guest_create_state'        => 'Erstelle Status-Feld',
    'guest_install_ok'          => 'Alles erfolgreich installiert',
    'guest_install_err'         => 'Installation fehlgeschlagen',
    'guest_root_pw'             => 'Root-User',
    'guest_user_name'           => 'Benutzername',
    'guest_user_name_data'      => '* darf nicht leer sein, maximal 30 Zeichen',
    'guest_password'            => 'Passwort',
    'guest_create_user'         => 'Benutzer anlegen',
    'guest_user_created'        => 'Benutzer erfolgreich angelegt',
    'guest_delete_install'      => 'Sie können nun den Installationsordner löschen.',
    'guest_user_create_err'     => 'Benutzer konnte nicht angelegt werden!'
    );
?>