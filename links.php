﻿<?php
    /**
      @file   links.php
      @author Ruhland Matthias

      @brief  Holds all available pages

      The visitor can enter a specific page in the url.
      Then the index.php will lookup the page from the url
      here in this file. If the page exists, the file on
      the filesystem will be loaded.
    */

    $links = array(
    'guestbook'     => 'guestbook.php'
    );
?>