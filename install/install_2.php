<?php
    /**
      @file   install_2.php
      @author Ruhland Matthias

      @brief  Creates your MySQL database, the tables and the root-user
    */

    /**
      @brief This functions adds some tags to the Head
    */
    function showHead() {
        include 'include_lang.php';

        echo '    <title>' , $lang['guest_install_title'] , '</title>' . "\n";
    }

    /**
      @brief This functions shows the content of the page
    */
    function showPage() {
        include 'include_lang.php';
        include '../settings/settings.php';
        include '../library/password.inc.php';

        $_SESSION['page'] = 'gb_install_install';

        echo '    <div id="install">' , "\n";

        echo '        <h1>' , $lang['guest_install_headline'] . '</h1>' , "\n";

        if ($_POST['create'] === false) {
            $db = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
            echo '        <p>' , $lang['guest_conn_db'] . ': ';
            if ($db === false) {
                echo $lang['guest_conn_err'] , '</p>' , "\n";
                return;
            } else
                echo $lang['guest_conn_ok'] , '</p>' , "\n";

            // Create a database if needed
            if ($guest_db_exist == 'no') {
                echo '        <p>' , $lang['guest_db_create'] . ': ';
                $db_create_db = mysql_query("create database $guest_database DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");
                if ($db_create_db === false) {
                    echo $lang['guest_db_create_err'] , '</p>' , "\n";
                    return;
                } else
                    echo $lang['guest_db_create_ok'] , '</p>' , "\n";
            }

            mysql_select_db($guest_database);
            echo '        <p>' , "\n";

            // Create the table and the id
            $db_id = mysql_query("create table $guest_table (id int not null auto_increment, index (id))");
            echo '            ' , $lang['guest_create_table'] , ': ';
            if ($db_id === false) {
                echo $lang['guest_error'] , '<br />' , "\n";
                return;
            } else
                echo $lang['guest_success'] , '<br />' , "\n";

            // Create the field for the name
            $db_name = mysql_query("alter table $guest_table add name varchar(30) not null");
            echo '            ' , $lang['guest_create_name'] , ': ';
            if ($db_name === false) {
                echo $lang['guest_error'] , '<br />' , "\n";
                return;
            }
                echo $lang['guest_success'] , '<br />' , "\n";

            // Create the field for the mail address
            $db_mail = mysql_query("alter table $guest_table add mail varchar(30)");
            echo '            ' , $lang['guest_create_mail'] , ': ';
            if ($db_mail === false) {
                echo $lang['guest_error'] , '<br />' , "\n";
                return;
            }
                echo $lang['guest_success'] , '<br />' , "\n";

            // Create the field for the message
            $db_message = mysql_query("alter table $guest_table add message text");
            echo '            ' , $lang['guest_create_message'] , ': ';
            if ($db_message === false) {
                echo $lang['guest_error'] , '<br />' , "\n";
                return;
            }
                echo $lang['guest_success'] , '<br />' , "\n";

            // Create the field for the time
            $db_time = mysql_query("alter table $guest_table add time varchar(16)");
            echo '            ' , $lang['guest_create_time'] , ': ';
            if ($db_time === false) {
                echo $lang['guest_error'] , '<br />' , "\n";
                return;
            }
                echo $lang['guest_success'] , '<br />' , "\n";

            // Create the field for the IP-Address
            $db_ip = mysql_query("alter table $guest_table add ip varchar(45)");
            echo '            ' , $lang['guest_create_ip'] , ': ';
            if ($db_ip === false) {
                echo $lang['guest_error'] , '<br />' , "\n";
                return;
            }
                echo $lang['guest_success'] , '<br />' , "\n";

            // Create the field for the state
            $db_state = mysql_query("alter table $guest_table add state varchar(45)");
            echo '            ' , $lang['guest_create_state'] , ': ';
            if ($db_state === false) {
                echo $lang['guest_error'] , '<br />' , "\n";
                return;
            }
                echo $lang['guest_success'] , '<br />' , "\n";

            echo '        </p>' , "\n";

            echo '        <h2>' , "\n";
            
            // Check if all was successfully done
            if ($db_id      === true && $db_name === true && $db_mail === true &&
                $db_message === true && $db_time === true && $db_ip   === true &&
                $db_state   === true) {
                // If all was successfully, only check the db_create
                if ($guest_db_exist == 'no')
                    ($db_create_db === true)?echo $lang['guest_install_ok':echo $lang['guest_install_err'];
                else
                    echo $lang['guest_install_ok'];

            } else
                echo $lang['guest_install_err'];

            echo '        </h2>' , "\n";

            // Create root-Password
            $user = mysql_query("CREATE TABLE $guest_usertable ( userid int(2) not null auto_increment,
                                 username varchar(30) NOT NULL UNIQUE, userpass text not null, userlevel int(1), index (userid) )");

            mysql_close($db);

            echo '        <form action="index.php?page=gb_install_install" method="post">' , "\n";
            echo '            <fieldset>' , "\n";
            echo '                <legend>' , $lang['guest_root_pw'] , '</legend>' , "\n";
            echo '                <label for="username">' , $lang['guest_user_name'] , ': </label>';
            echo '<input name="username" size="30" id="username" /> ' , $lang['guest_user_name_data'] , '<br />' , "\n";
            echo '                <label for="password">' , $lang['guest_password'] , ': </label>';
            echo '<input name="password" type="password" size="30" id="password" /><br />' , "\n";
            echo '                <input type="submit" value="' , $lang['guest_create_user'] , '" name="create" />' , "\n";
            echo '            </fieldset>' , "\n";
            echo '        </form>' , "\n";
        }

        if ($_POST['create'] === true) {
            $db        = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
            mysql_select_db($guest_database);
            $password  = password_create($_POST['password']);
            $user      = mysql_real_escape_string($_POST['username']);
            $root      = mysql_query("insert into $guest_usertable set username='$user',
                                      userpass='$password', userlevel=3");

            echo '        <p>' , "\n";
            if ($root === true) {
                echo '            ' , $lang['guest_user_created'] , '<br />' , "\n";
                echo '            ' , $lang['guest_delete_install'] , '<br />' , "\n";
            }
            else
                echo '            ' , $language['guest_user_create_err'] , '<br />' , "\n";

            echo '        </p>' , "\n";
        }

        echo '    </div>' , "\n";
    }
?>