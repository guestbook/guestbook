<?php
    /**
      @file   index.php
      @author Ruhland Matthias

      @brief  The main-page

      This page holds the html-header of the site
      and includes other page-specific html-header-tags
      and also the content of the site.
    */

    session_start();                // Start session to store users language and actual page

    error_reporting(E_ALL);            // Show all errors
    //error_reporting(0);                // Show no errors

    include '../library/check_lang.php';
    include 'include_lang.php';
    include 'include_page.php';

    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">' , "\n";
    echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="' , $lang['lang'] , '" lang="' , $lang['lang'] , '">' , "\n";

    echo '<head>' , "\n";

    echo '    <meta name="author" content="Ruhland Matthias" />' , "\n";
    echo '    <meta name="publisher" content="Ruhland Matthias" />' , "\n";
    echo '    <meta name="robots" content="noindex,noarchive" />' , "\n";
    echo '    <meta name="revisit-after" content="2 weeks" />' , "\n";
    echo '    <meta http-equiv="expires" content="0" />' , "\n";
    echo '    <meta http-equiv="cache-control" content="no-cache" />' , "\n";
    echo '    <meta http-equiv="pragma" content="no-cache" />' , "\n";
    echo '    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />' , "\n";
    echo '    <meta http-equiv="content-language" content="' , $lang['lang'] , '" />' , "\n";
    echo '    <meta http-equiv="content-style-type" content="text/css" />' , "\n";

    echo '    <link rel="stylesheet" href="../styles/guestbook.css" type="text/css" />' , "\n";

    // You can include here the CSS of your main-page

    showHead();                    // Show the page-specific header

    echo '</head>' , "\n";
    echo '<body>' , "\n";
    echo '<div id="main">' , "\n";

    // Main-File
    showPage();                // Show the page

    // Footer
    // If you don't need the footer at the bottom of the page, you
    // only have to comment out the next 3 lines.
    echo '    <div id="footer">' , "\n";
    include '../footer.html';
    echo '    </div>' , "\n";

    echo '</div>' , "\n";
    echo '</body>' , "\n";
    echo '</html>' , "\n";
?>