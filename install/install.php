<?php
    /**
      @file   install.php
      @author Ruhland Matthias

      @brief  Shows infomation about your settings.

      The page for installing the guestbook is set to start, so others can't search with
      Google pages like index.php?page=install or something like this. If you search for
      the start-page, Google should find mostly guestbooks.

      After installing the guestbook you should remove the folder and all files, so others
      can't reinstall it and change your root-password.
    */

    /**
      @brief This functions adds some tags to the Head
     */
    function showHead() {
        include 'include_lang.php';

        echo '    <title>' , $lang['guest_install_title'] , '</title>' , "\n";
    }

    /**
      @brief This functions shows the content of the page
     */
    function showPage() {
        include 'include_lang.php';
        include '../settings/settings.php';

        $_SESSION['page'] = 'gb_install_start';

        echo '    <div id="install">' , "\n";

        echo '        <h1>' , $lang['guest_install_headline'] , '</h1>' , "\n";
        echo '        <p>' , $lang['guest_install_attention'] , '</p>' , "\n";

        // Show all settings
        echo '        <p><strong>' , $lang['guest_install_dbs'] , '</strong><br />' , "\n";
        echo '        <table>' , "\n";
        echo '            <tr><td>' , $lang['guest_host'] , ': </td><td>$guest_db_host</td></tr>' , "\n";
        echo '            <tr><td>' , $lang['guest_user'] , ': </td><td>$guest_db_user</td></tr>' , "\n";
        echo '            <tr><td>' , $lang['guest_pass'] , ': </td><td>$guest_db_passwd</td></tr>' , "\n";
        echo '            <tr><td>' , $lang['guest_db_exist'] , ': </td><td>';
        if ($guest_db_exist === 'yes') echo $lang['guest_yes'];
        else                           echo $lang['guest_no'];
        echo '</td></tr>' , "\n";
        echo '        </table>' , "\n";

        echo '        <p><strong>' , $lang['guest_mail_check'] , '</strong> <em>';
        if ($guest_set_check === 'yes') echo $lang['guest_yes'];
        else                            echo $lang['guest_no'];
        echo '</em></p>' , "\n";

        echo '        <p>' , $lang['guest_password_information'] , '</p>' , "\n";

        echo '        <p><strong><a href="index.php?page=gb_install_install">';
        echo $lang['guest_do_install'] , '</a></strong></p>' , "\n";

        echo '    </div>' , "\n";
    }
?>