<?php
    /**
     @file   include_page.php
     @author Ruhland Matthias

     @brief  Includes the page-file
    */

    include 'links.php';

    if (isset($_GET['page']) === false) {                        // Visitor won't a special page (fist visit, change lang)
        if (isset($_SESSION['page']) === false)                    // No site stored in the session (first visit)
            $page = $links['gb_install_start'];            // Index-page
        else
            $page = (isset($links[$_SESSION['page']]) === true)?$links[$_SESSION['page']]:$links['gb_install_start'];
    } else {
        $page = (isset($links[$_GET['page']]) === true)?$page = $links[$_GET['page']]:$links['gb_install_start'];
    }

    include $page;        // include the page the user want to see
?>