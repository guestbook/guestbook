<?php
    /**
      @file actual_page.php
      @author Ruhland Matthias

      @brief Checks if the page_name equals to the current page
     */

    /**
     @brief Checks if the page is active

     First check if there is the pagename in the URL or in the session (else no page selected)
     If no page is set, check if the pagename is the indexpage

     @param page_name the name of the page to check if it is the actual page

     @return true if the page_name is the actual shown page
     else return false
     */
    function check_actual_page($page_name) {
        include 'links.php';
        if (isset($_GET['page'])) {          // New page selected
            if ($_GET['page'] === $page_name)
                return true;

            if (isset($links[$_GET['page']]) === false && $page_name === 'start')
                return true;
            else
                return false;
        }

        if (isset($_SESSION['page'])) {      // no new page selected (language changed)
            if ($_SESSION['page'] === $page_name)
                return true;
            else
                return false;
        } else if (isset($_GET['page']) === false && isset($_SESSION['page']) === false && $page_name === 'start')
            return true;
        else
            return false;
    }
?>