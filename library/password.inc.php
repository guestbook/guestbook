<?php
    /**
      @file   password.inc.php
      @author Ruhland Matthias

      @brief  Some functions to create and verify the password-hash

      If in later Versions of PHP the password-hashing changes,
      or if your PHP-Version not supports the current hash,
      then you can change it here.
     */

    /**
      @brief Creates a password-hash

      @param password The password to create a hash

      With this function it is easily possible to change
      the method to create a password-hash.

      @returns the hash-String
     */
    function password_create($password)
    {
        //return md5($password);    // simple, but unsecure!
        return crypt($password);    // available at least with PHP 5.3.0
    }

    /**
      @brief Check the password-hashes

      @param password The password to create a hash
      @param hash     The hash of the password

      Creates the hash of the password using the password_create-function.

      @returns true if the hashes are the same, else false
     */
    function password_check($password, $hash)
    {
        //if (password_create($password) == $hash)
        /*if (crypt($password, $hash) === $hash)
            return true;
        else
            return false;*/
        return (crypt($password, $hash) === $hash)?true:false;
    }
?>