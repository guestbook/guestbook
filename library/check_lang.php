﻿<?php
    /**
      @file   check_lang.php
      @author Ruhland Matthias

      @brief  Checks the users language

      If there is no session set check the cookies. If there is a language stored, use it.
      Else check the Browserlanguage.

      If we got no prefered, available language use english (en).

      But if there is no session set and the file is included again,
      the user want another language. First check it if the language
      is available and store the lang in a cookie, so in the future
      he will get his prefered language. Also store the language in the
      session, so he will get other sites also in his prefered lang.
    */

    // If no session exists (new visitor?), check cookie, else check browser-language
    if (!isset($_SESSION['lang']) === true) {
        if (isset($_COOKIE['lang']) === true) {
            $_SESSION['lang'] = ($_COOKIE['lang'] === 'de')?'de':'en';
            //if ($_COOKIE['lang'] == "de")
            //    $_SESSION['lang'] = "de";
            //else
            //    $_SESSION['lang'] = "en";
        } else if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) === true) {
            if ($_SERVER['HTTP_ACCEPT_LANGUAGE'] !=== '') {
                $accept_language = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
                $accept_language = strtolower($accept_language);
                $accept_language = str_replace(' ', '', $accept_language);
                $accept_language = explode(',', $accept_language);

                foreach ($accept_language as $language_list) {
                    $lang_part = substr($language_list, 0, 2);
                    if ($lang_part === 'de') {
                        $_SESSION['lang'] = 'de';
                        break;
                    } else if ($lang_part === 'en') {
                        $_SESSION['lang'] = 'en';
                        break;
                    }
                }
            }
        }
    }

    // Default lang, if no other session_lang is set, or no other cookie was found, and no Browser-language was found
    if (isset($_SESSION['lang']) === false)
        $_SESSION['lang'] = 'de';

    // Check language in URL and save language in a cookie
    if (isset($_GET['lang']) === true) {
        if ($_GET['lang'] == 'de') {
            $_SESSION['lang'] = 'de';
            setcookie('lang', 'de', time()*60*60*24*7*52, '/');
        } else {
            $_SESSION['lang'] = 'en';
            setcookie('lang', 'en', time()*60*60*24*7*52, "/");
        }
    }
?>