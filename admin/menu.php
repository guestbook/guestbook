﻿<?php
    /**
	  @file   menu.php
	  @author Ruhland Matthias

	  @brief  The menu of the page
	*/
    include "include_lang.php";

    /**
      @brief Checks if the page is active

      First check if there is the pagename in the URL or in the session (else no page selected)
      If no page is set, check if the pagename is the indexpage

      @param page_name the name of the page to check if it is the actual page

      @return true if the page_name is the actual shown page
              else return false
     */
    function check_actual_page($page_name)
    {
        include "links.php";

        if (isset($_GET['page']))			// New page selected
        {
        	if ($_GET['page'] == $page_name)
        		return true;

        	if (!isset($links[$_GET['page']]) && $page_name == "gb_admin_start")
        		return true;
            else
            	return false;
        }

        if (isset($_SESSION['page']))		// no new page selected (language changed)
        {
            if ($_SESSION['page'] == $page_name)
            	return true;
            else
            	return false;
        }
        else if (!isset($_GET['page']) && !isset($_SESSION['page']) && $page_name == "gb_admin_start")
        	return true;
        else
        	return false;
    }

    // Check the session-ip and the browser-ip
	if (isset($_SESSION['ip']))
	{
        if ($_SESSION['ip'] == $_SERVER['REMOTE_ADDR'])
            $userlevel = $_SESSION['userlevel'];
    }
    else
        $userlevel = 0;

    echo "<ul>\n";
    // Not loggedin
	if ($userlevel == 0)
	{
		echo "<li class=\"aktiv\"><a href=\"index.php?page=gb_admin_login\">";
		echo $lang['guest_login'] . "</a></li>\n";
	}

    // Loggedin
	if ($userlevel >= 1)
	{
		echo "<li";
        if (check_actual_page("gb_admin_logout")) echo " class=\"aktiv\"";
		echo "><a href=\"index.php?page=gb_admin_logout\">" . $lang['guest_logout'] . "</a></li>\n";
		echo "<li";
		if (check_actual_page("gb_admin_entry")) echo " class=\"aktiv\"";
		echo "><a href=\"index.php?page=gb_admin_entry\">" . $lang['guest_entries'] . "</a></li>\n";

		// Loggedin, but every user can change his own password
		echo "<li";
		if (check_actual_page("gb_admin_password")) echo " class=\"aktiv\"";
		echo "><a href=\"index.php?page=gb_admin_password\">" . $lang['guest_passwd_chk'] . "</a></li>\n";
	}

    // With userlevel above 2 you can administrate other users
	if ($userlevel >= 2)
	{
		echo "<li";
        if (check_actual_page("gb_admin_users")) echo " class=\"aktiv\"";
		echo "><a href=\"index.php?page=gb_admin_users\">" . $lang['guest_users_admin'] . "</a></li>\n";
	}

	echo "</ul>\n";
?>