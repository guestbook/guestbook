<?php
	/**
	 @file   include_page.php
	 @author Ruhland Matthias

	 @brief  Includes the page-file
	*/

	include "links.php";

	if (!isset($_GET['page']))						// Visitor won't a special page (fist visit, change lang)
	{
		if (!isset($_SESSION['page']))				// No site stored in the session (first visit)
			$page = $links['gb_admin_start'];		// Index-page
		else
		{
			if (isset($links[$_SESSION['page']]))	// If visitors site is registered, load it
				$page = $links[$_SESSION['page']];
			else									// Else load the index-page
				$page = $links['gb_admin_start'];
		}
	}
	else
	{
		if (isset($links[$_GET['page']]))			// If the page is registered, load it
			$page = $links[$_GET['page']];
		else										// Else show him the index-page
			$page = $links['gb_admin_start'];
	}

	include $page;		// include the page the user want to see
?>