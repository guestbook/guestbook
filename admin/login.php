﻿<?php
	/**
	  @file   login.php
	  @author Ruhland Matthias

	  @brief  Login-page

      @todo write a refresh when user logged in (isset(POST) in Header and set refresh 0 page=start?)
	*/

	/**
	  @brief Shows additional info in the HTML-Header
	 */
	function showHead()
	{
		include "include_lang.php";

		echo "<title>". $lang['guest_login_title'] . "</title>\n";
	}

	/**
	  @brief Shows the page
	 */
	function showPage()
	{
    	include "include_lang.php";
    	include "../library/password.inc.php";

    	$_SESSION['page'] = 'gb_admin_login';

    	echo "<div id=\"admin\">\n";

    	echo "<h1>" . $lang['guest_user'] . " - " . $lang['guest_login'] . "</h1>\n";

        // If you are not logged in, show the login-formular
        if (isset($_POST['login']) == false)
        {
            echo "<form action=\"index.php?page=login\" method=\"post\">\n";
            echo "<fieldset>\n";
            echo "<legend>" . $lang['guest_login'] . "</legend>\n";
            echo "<label for=\"username\">" . $lang['guest_user_name'] . ": </label>";
            echo "<input name=\"username\" size=\"30\" id=\"username\"/><br />\n";
            echo "<label for=\"password\">" . $lang['guest_password'] . ": </label>";
            echo "<input name=\"password\" type=\"password\" size=\"30\" id=\"password\"/><br />\n";
            echo "<input type=\"submit\" value=\"" . $lang['guest_login'] . "\" name=\"login\"/>\n";
            echo "<input type=\"reset\"  value=\"" . $lang['guest_clear'] . "\"/>\n";
            echo "</fieldset>\n";
            echo "</form>\n";
        }

        // If you log in, check your data
        if (isset($_POST['login']) == true)
        {
        include "../settings/settings.php";

		$db   = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
		$user = mysql_real_escape_string($_POST['username']);
		mysql_select_db($guest_database);
		$res  = mysql_query("select * from $guest_usertable where username='$user'");
		$db_user     = mysql_result($res, 0, "username");
		$db_password = mysql_result($res, 0, "userpass");
		$db_level    = mysql_result($res, 0, "userlevel");

		if ($user == $db_user && password_check($_POST['password'], $db_password))
		{
            // Write to the session
			$_SESSION['userlevel'] = $db_level;
			$_SESSION['user']      = $db_user;
			$_SESSION['ip']        = $_SERVER['REMOTE_ADDR'];
			echo "<p>" . $lang['guest_login_ok'];
			echo "<a href=\"index.php\"> " . $lang['guest_login_here'] . "</a></p>\n";
		}
		else
			echo "<p>" . $lang['guest_login_err'] . "</p>\n";

		mysql_close($db);
	}

	    echo "</div>\n";
	}
?>