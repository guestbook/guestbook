﻿<?php
	/**
	  @file   entries.php
	  @author Ruhland Matthias

	  @brief  Administrate all entries in the guestbook
	*/

	/**
	 @brief This functions adds some tags to the Head
	 */
    function showHead()
    {
		include "include_lang.php";

		echo "<title>". $lang['guest_login_title'] . "</title>\n";
	}

	/**
	 @brief This functions shows the content of the page
	 */
    function showPage()
    {
    	include "include_lang.php";
        include "../settings/settings.php";

    	$_SESSION['page'] = 'gb_admin_entries';

    	echo "<div id=\"admin\">\n";

    	echo "<h1>" . $lang['guest_entries'] . "</h1>\n";

        if (isset($_SESSION['userlevel']))
        {
            $userlevel = $_SESSION['userlevel'];

            if ($userlevel >= 1 && $_SESSION['ip'] == $_SERVER['REMOTE_ADDR'])
            {
                $db = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);

                if (isset($_GET['delete']) == true)
                {
                    $del_id     = mysql_escape_string($_GET['id']);
                    mysql_select_db($guest_database);
                    $delete     = "delete from $guest_table ";
                    $delete    .= " where id = $del_id";
                    mysql_query($delete);
                    $num = mysql_affected_rows();
                    echo "<p>";
                    if ($num > 0) echo $lang['guest_entry_deleted'];
                    else          echo $lang['guest_entry_deleted_err'];
                    echo "</p>\n";
                }

                if (isset($_GET['show']) == true)
                {
                    $show_id   = mysql_real_escape_string($_GET['id']);
                    mysql_select_db($guest_database);
                    $update    = "update $guest_table set state = '1' where id = $show_id";
                    mysql_query($update);
                    $num       = mysql_affected_rows();
                    echo "<p>";
                    if ($num > 0) echo $lang['guest_show_entry'];
                    else          echo $lang['guest_show_entry_err'];
                    echo "</p>\n";
                }

                if (isset($_GET['hide']) == true)
                {
                    $hide_id   = mysql_real_escape_string($_GET['id']);
                    mysql_select_db($guest_database);
                    $update    = "update $guest_table set state = '0' where id = $hide_id";
                    mysql_query($update);
                    $num       = mysql_affected_rows();
                    echo "<p>";
                    if ($num > 0) echo $lang['guest_hide_entry'];
                    else          echo $lang['guest_hide_entry_err'];
                    echo "</p>\n";
                }

                mysql_select_db($guest_database);
                $res        = mysql_query("select count(*) as counter from $guest_table");
                $counter    = mysql_fetch_array($res);
                $counter    = $counter['counter'];
                $page_count = ceil($counter / $guest_entr_page);

                if (isset($_GET['page'])) $page = $_GET['page'];
                else                      $page = 0;
                if ($page > $page_count)  $page = 0;
                $start = $page * $guest_entr_page;

                mysql_select_db($guest_database);
                $res = mysql_query("select * from $guest_table order by time desc
                                    LIMIT $start, $guest_entr_page");

                $num = mysql_num_rows($res);

                if ($page_count > 1)
                {
                    echo "<p>" . $lang['guestbook_page'];
                    $server = $_SERVER['PHP_SELF'];
                    for ($i = 0; $i < $page_count; $i++)
                    {
                        echo "<a href=\" ". $server . "?page=gb_admin_entry&amp;page=";
                        echo $i . "\"> " . ($i + 1) . " </a>";
                    }
                    echo "<p/>\n";
                }

                echo "<table>\n";
                for ($i = 0; $i < $num; $i++)
                {
                    $id      = mysql_result($res, $i, "id");
                    $name    = mysql_result($res, $i, "name");
                    $mail    = mysql_result($res, $i, "mail");
                    $message = mysql_result($res, $i, "message");
                    $time    = mysql_result($res, $i, "time");
                    $ip      = mysql_result($res, $i, "ip");
                    $state   = mysql_result($res, $i, "state");

                    echo "<tr>";
                    echo "<td class=\"entries_id\">$id</td>\n";
                    echo "<td class=\"entries_name\">$name</td>\n";
                    echo "<td class=\"entries_mail\">$mail</td>\n";
                    echo "<td class=\"entries_state\">";

                    echo "<a href=\" ". $_SERVER['PHP_SELF'] . "?page=gb_admin_entry&amp;";

                    if ($state == "1")				// State set to show
                        echo "hide=true&amp;id=" . $id . "\"> " . $lang['guest_hide'] . "</a>";
                    else 				        // State set to hide
                        echo "show=true&amp;id=" . $id . "\"> " . $lang['guest_show'] . "</a>";

                    echo "</td>\n";
                    echo "</tr>\n";
                    echo "<tr>\n";
                    echo "<td class=\"entries_message\">";
                    $message    = utf8_encode($message);
                    $message_br = nl2br($message);
                    echo $message_br;
                    echo "</td>\n";
                    echo "<td class=\"entries_time\">$time</td>\n";
                    echo "<td class=\"entries_ip\">$ip</td>\n";
                    echo "<td class=\"entries_del\">";
                    echo "<a href=\" ". $_SERVER['PHP_SELF'];
                    echo "?page=gb_admin_entry&amp;delete=true&amp;id=" . $id . "\"> ";
                    echo $lang['guest_delte_entry'] . " </a>";
                    echo "</td>\n";
                    echo "</tr>\n";
                }
                echo "</table>\n";

                mysql_close($db);
            }
            else
                echo $lang['guest_user_not_valid'] . "\n";
        }
        else
            echo $lang['guest_user_not_loggedin'] . "\n";

	    echo "</div>\n";
	}
?>