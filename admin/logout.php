﻿<?php
    /**
	  @file   logout.php
	  @author Ruhland Matthias

	  @brief  Logging out

      @todo write a refresh when user logged out (isset(POST) in Header and set refresh 0 page=start?)

	  @changes
	*/

	/**
	 @brief This functions adds some tags to the Head
	 */
	function showHead()
	{
		include "include_lang.php";

		echo "<title>". $lang['guest_login_title'] . "</title>\n";
	}

	/**
	 @brief This functions shows the content of the page
	 */
	function showPage()
	{
    	include "include_lang.php";

    	$_SESSION['page'] = 'gb_admin_logout';

    	echo "<div id=\"admin\">\n";

    	echo "<h1>" . $lang['guest_user'] . " - " . $lang['guest_logout'] . "</h1>\n";

        unset($_SESSION['userlevel']);
        unset($_SESSION['user']);
        unset($_SESSION['ip']);
        session_destroy();
        echo $lang['guest_logged_out'];
        echo "<a href=\"index.php?page=gb_admin_start\"> " . $lang['guest_login_here'] . "</a>\<br />\n";

	    echo "</div>\n";
	}
?>