﻿<?php
	/**
	  @file   password.php
	  @author Ruhland Matthias

	  @brief  Change your password
	*/

	/**
	 @brief This functions adds some tags to the Head
	 */
	function showHead()
	{
		include "include_lang.php";

		echo "    <title>". $lang['guest_login_title'] . "</title>\n";
	}

	/**
	 @brief This functions shows the content of the page
	 */
	function showPage()
	{
        include "include_lang.php";
        include "../settings/settings.php";

    	$_SESSION['page'] = 'gb_admin_password';

    	echo "<div id=\"admin\">\n";

        echo "<h1>" . $lang['guest_passwd_chk'] . "</h1>\n";

        if (isset($_SESSION['userlevel']))
        {
            $userlevel = $_SESSION['userlevel'];

            if ($userlevel >= 1 && $_SESSION['ip'] == $_SERVER['REMOTE_ADDR'])
            {
                $user = $_SESSION['user'];

                if (isset($_POST['change']) == false)
                {
                    echo "<form action=\"index.php?page=gb_admin_password\" method=\"post\">\n";
                    echo "<fieldset>\n";
                    echo "<legend>" . $lang['guest_passwd_chk'] . "</legend>\n";
                    echo "<label for=\"pass_old\">" . $lang['guest_pass_old'] . ": </label>";
                    echo "<input name=\"pass_old\" type=\"password\" size=\"30\" id=\"pass_old\"/><br />\n";
                    echo "<label for=\"pass_new_1\">" . $lang['guest_pass_new'] . ": </label>";
                    echo "<input name=\"pass_new_1\" size=\"30\" id=\"pass_new_1\"/><br />\n";
                    echo "<label for=\"pass_new_2\">" . $lang['guest_pass_new'] . ": </label>";
                    echo "<input name=\"pass_new_2\" size=\"30\" id=\"pass_new_2\"/><br />\n";
                    echo "<input type=\"submit\" value=\"" . $lang['guest_passwd_chk'] . "\" name=\"change\"/>\n";
                    echo "<input type=\"reset\"  value=\"" . $lang['guest_cancel'] . "\"/>\n";
                    echo "</fieldset>\n";
                    echo "</form>\n";
                }

                if (isset($_POST['change']) == true)
                {
                    if ($_POST['pass_new_1'] == $_POST['pass_new_2'])
                    {
                        $user    = $_SESSION['user'];
                        $db      = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
                        mysql_select_db($guest_database);
                        $res     = mysql_query("select * from $guest_table where username='$user'");
                        $new_pw  = md5($_POST['pass_new_1']);
                        $db_user = mysql_result($res, 0, "username");
                        $db_pass = mysql_result($res, 0, "userpass");
                        if ($new_pw == $db_pass)
                            echo $lang['guest_same_pw'] . "\n";
                        else
                        {
                            if ($user == $db_user)
                            {
                                $db_change = "use $guest_database";
                                $db_change = mysql_query($db_change);
                                $res = "update $guest_table set userpass = '$new_pw' where username = '$user'";
                                $res = mysql_query($res);
                                $num = mysql_affected_rows();
                                echo "<p>";
                                if ($num > 0) echo $lang['guest_pw_chked'];
                                else          echo $lang['guest_pw_chked_err'];
                                echo "</p>\n";
                            }
                        }
                        mysql_close($db);
                    }
                }
            }
            else
                echo $lang['guest_user_not_valid'] . "\n";
        }
        else
            echo $lang['guest_user_not_loggedin'] . "\n";

	    echo "    </div>\n";
	}
?>