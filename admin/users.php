﻿<?php
	/**
	  @file   users.php
	  @author Ruhland Matthias

	  @brief  Administrate all users
	*/

	/**
	  @brief This functions adds some tags to the Head
	*/
    function showHead()
    {
		include "include_lang.php";

		echo "    <title>". $lang['guest_login_title'] . "</title>\n";
	}

	/**
	 @brief This functions shows the content of the page
	 */
    function showPage()
    {
    	include "include_lang.php";
        include "../settings/settings.php";
        include "../library/password.inc.php";

    	$_SESSION['page'] = 'gb_admin_users';

    	echo "    <div id=\"admin\">\n";

    	echo "        <h1>" . $lang['guest_user'] . "</h1>\n";

        if (isset($_SESSION['userlevel']))
        {
            $userlevel = $_SESSION['userlevel'];

            if ($userlevel >= 2 && $_SESSION['ip'] == $_SERVER['REMOTE_ADDR'])
            {
                $user = $_SESSION['user'];

                // Form to create a user
                if (isset($_POST['create_user']) == true)
                {
                    echo "            <form action=\"index.php?page=gb_admin_users\" method=\"post\">\n";
                    echo "                <fieldset>\n";
                    echo "                    <legend>" . $lang['guest_create_user'] . "</legend>\n";
                    echo "                    <label for=\"user\">" . $lang['guest_user_name'] . ": </label>";
                    echo "<input name=\"user\" size=\"30\" id=\"user\"/><br />\n";
                    echo "                    <label for=\"pass\">" . $lang['guest_password'] . ": </label>";
                    echo "<input name=\"pass\" type=\"password\" size=\"30\" id=\"pass\"/><br />\n";
                    echo "                    <label for=\"pass_new_2\">" . $lang['guest_password'] . ": </label>";
                    echo "<input name=\"pass_new_2\" type=\"password\" size=\"30\" id=\"pass_new_2\"/><br />\n";
                    echo "                    <label for=\"level\">" . $lang['guest_userlevel'] . ": </label>";
                    echo "<select name=\"userlevel\" id=\"level\">\n";
                    echo "                        <option value = \"1\"> ". $lang['guest_userlevel_1'] . "</option>\n";
                    echo "                        <option value = \"2\"> ". $lang['guest_userlevel_2'] . "</option>\n";
                    echo "                    </select><br />\n";
                    echo "                    <input type=\"submit\" value=\"" . $lang['guest_create'] . "\" name=\"create\"/>\n";
                    echo "                    <input type=\"reset\"  value=\"" . $lang['guest_cancel'] . "\"/>\n";
                    echo "                </fieldset>\n";
                    echo "            </form>\n";
                }

                // Create a user
                if (isset($_POST['create']) == true)
                {
                    $user  = mysql_real_escape_string($_POST['user']);
                    $pass  = password_create($_POST['pass']);
                    $level = mysql_real_escape_string($_POST['userlevel']);
                    $db    = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
                    mysql_select_db($guest_database);
                    $root  = mysql_query("insert into $guest_usertable set username='$user', userpass='$pass', userlevel=$level");
                    $num   = mysql_affected_rows();
                    echo "        <p>";
                    if ($num > 0) echo $lang['guest_created'];
                    else        echo $lang['guest_created_err'];
                    echo "</p>\n";
                    mysql_close($db);
                }

                // Form to change the userlevel
                if (isset($_GET['userlevel']) == true)
                {
                    $userid = mysql_real_escape_string($_GET['id']);
                    echo "            <form action=\"index.php?page=gb_admin_users\" method=\"post\">\n";
                    echo "                <fieldset>\n";
                    echo "                    <label for=\"level\">" . $lang['guest_userlevel'] . ": </label>";
                    echo "<select name=\"userlevel\" id=\"level\">\n";
                    echo "                        <option value = \"1\"> ". $lang['guest_userlevel_1'] . "</option>\n";
                    echo "                        <option value = \"2\"> ". $lang['guest_userlevel_2'] . "</option>\n";
                    echo "                    </select><br />\n";
                    echo "                    <input name=\"id\" type=\"hidden\" value=\"" . $userid . "\"/><br />\n";
                    echo "                    <input type=\"submit\" value=\"" . $lang['guest_change'] . "\" name=\"chk_userlevel\"/>\n";
                    echo "                    <input type=\"reset\"  value=\"" . $lang['guest_cancel'] . "\"/>\n";
                    echo "                </fieldset>\n";
                    echo "            </form>\n";
                }

                // change the userlevel
                if (isset($_POST['chk_userlevel']) == true)
                {
                    $userid = mysql_real_escape_string($_POST['id']);
                    $level  = mysql_real_escape_string($_POST['userlevel']);
                    $db     = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
                    mysql_select_db($guest_database);
                    $res    = "update $guest_usertable set userlevel = '$level' where userid = '$userid'";
                    $res    = mysql_query($res);
                    $num    = mysql_affected_rows();
                    echo "        <p>";
                    if ($num > 0) echo $lang['guest_level_chked'];
                    else          echo $lang['guest_level_chked_err'];
                    echo "</p>\n";
                    mysql_close($db);
                }

                // Form to change password
                if (isset($_GET['password']) == true)
                {
                    $userid = mysql_real_escape_string($_GET['id']);
                    echo "            <form action=\"index.php?page=gb_admin_users\" method=\"post\">\n";
                    echo "                <fieldset>\n";
                    echo "                    <legend>" . $lang['guest_passwd_chk'] . "</legend>\n";
                    echo "                    <label for=\"pass\">" . $lang['guest_pass'] . ": </label>";
                    echo "<input name=\"pass\" type=\"password\" size=\"30\" id=\"pass\"/><br />\n";
                    echo "                    <input name=\"id\" type=\"hidden\" value=\"" . $userid . "\"/><br />\n";
                    echo "                    <input type=\"submit\" value=\"" . $lang['guest_change'] . "\" name=\"chk_pass\"/>\n";
                    echo "                    <input type=\"reset\"  value=\"" . $lang['guest_cancel'] . "\"/>\n";
                    echo "                </fieldset>\n";
                    echo "            </form>\n";
                }

                // Change password
                if (isset($_POST['chk_pass']) == true)
                {
                    $userid = mysql_real_escape_string($_POST['id']);
                    $pass   = password_create($_POST['pass']);
                    $db     = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
                    mysql_select_db($guest_database);
                    $res    = "update $guest_usertable set userpass = '$pass' where userid = '$userid'";
                    $res    = mysql_query($res);
                    $num    = mysql_affected_rows();
                    echo "        <p>";
                    if ($num > 0) echo $lang['guest_pw_chked'];
                    else          echo $lang['guest_pw_chked_err'];
                    echo "</p>\n";
                    mysql_close($db);
                }
//! @todo remove all over the code the md5()-function
                // Delete user
                if (isset($_GET['delete']) == true)
                {
                    $userid = mysql_real_escape_string($_GET['id']);
                    $db     = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
                    mysql_select_db($guest_database);
                    $res    = "delete from $guest_usertable where userid = $userid";
                    $res    = mysql_query($res);
                    $num    = mysql_affected_rows();
                    if ($num > 0) echo $lang['guest_user_del'];
                    else          echo $lang['guest_user_del_err'];
                    mysql_close($db);
                }

                if (isset($_POST['create_user']) == false && isset($_POST['userlevel']) == false)
                {
                    echo "            <form action=\"index.php?page=gb_admin_users\" method=\"post\">\n";
                    echo "                <input type=\"submit\" value=\"";
                    echo $lang['guest_create_user'] . "\" name=\"create_user\"/>\n";
                    echo "            </form>\n";
                }

                $db      = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
                mysql_select_db($guest_database);
                $res     = mysql_query("select count(*) as counter from $guest_usertable");
                $counter = mysql_fetch_array($res);
                $counter = $counter['counter'];
                $page_count = ceil($counter / $guest_entr_page);

                if (isset($_GET['page'])) $page = $_GET['page'];
                else                      $page = 0;
                if ($page > $page_count) $page = 0;

                $start = $page * $guest_entr_page;

                mysql_select_db($guest_database);
                $res     = mysql_query("select * from $guest_usertable order by userid asc
                                        LIMIT $start, $guest_entr_page");
                $num     = mysql_num_rows($res);

                if ($page_count > 1)
                {
                    echo "        <p>" . $lang['guestbook_page'];
                    $server = $_SERVER['PHP_SELF'];
                    for ($i = 0; $i < $page_count; $i++)
                    {
                        echo "<a href=\" ". $server . "?page=gb_admin_users&amp;page=" . $i . "\"> " . ($i + 1) . " </a>";
                    }
                    echo "<p/>\n";
                }

                // Anzeige aufbauen
                echo "        <table>\n";
                echo "<tr><th>" . $lang['guest_user_id'] . "</th>";
                echo "<th>" . $lang['guest_user'] . "</th>";
                echo "<th>" . $lang['guest_userlevel'] . "</th>";
                //echo "<th>" . $lang['guest_user'] . "</th>";
                //echo "<th>" . $lang['guest_user'] . "</th></tr>\n";
                echo "<th>&nbsp;</th><th>&nbsp;</th></tr>\n";
                for ($i = 0; $i < $num; $i++)
                {
                	$userid    = mysql_result($res, $i, "userid");
                	$username  = mysql_result($res, $i, "username");
                	$userlevel = mysql_result($res, $i, "userlevel");
                	echo "<tr><td>$userid</td>";
                	echo "<td>$username</td>";
                	echo "<td>$userlevel <a href=\" ". $_SERVER['PHP_SELF'];
                	echo "?page=gb_admin_users&amp;userlevel=true&amp;id=" . $userid . "\">";
                	echo $lang['guest_change'] . " </a></td>";
                	echo "<td><a href=\" ". $_SERVER['PHP_SELF'];
                	echo "?page=gb_admin_users&amp;password=true&amp;id=" . $userid . "\"> ";
                	echo $lang['guest_change'] . " </a></td>";
                	echo "<td><a href=\" ". $_SERVER['PHP_SELF'];
                	echo "?page=gb_admin_users&amp;delete=true&amp;id=" . $userid . "\"> ";
                	echo $lang['guest_delte_entry'] . " </a></td></tr>\n";
                }
                echo "        </table>\n";

                mysql_close($db);
            }
            else
                echo $lang['guest_user_not_valid'] . "\n";
        }
        else
            echo $lang['guest_user_not_loggedin'] . "\n";

	    echo "    </div>\n";
	}
?>