﻿<?php
	/**
	  @file   new.php
	  @author Ruhland Matthias

	  @brief  A new Entry was made
	*/

	session_start();				// Start session to store users language and actual page

    error_reporting(E_ALL);			// Show all errors
    //error_reporting(0);				// Show no errors

    include "../library/check_lang.php";
    include "include_lang.php";
    include "../settings/settings.php";

	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
	echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"" . $lang['lang'] . "\" lang=\"" . $lang['lang'] . "\">\n";

	echo "<head>\n";

    echo "<meta name=\"author\" content=\"Ruhland Matthias\" />\n";
    echo "<meta name=\"publisher\" content=\"Ruhland Matthias\" />\n";
    echo "<meta name=\"robots\" content=\"nofollo, noindex\" />\n";
    echo "<meta name=\"revisit-after\" content=\"2 weeks\" />\n";
    echo "<meta http-equiv=\"expires\" content=\"0\" />\n";
    echo "<meta http-equiv=\"cache-control\" content=\"no-cache\" />\n";
    echo "<meta http-equiv=\"pragma\" content=\"no-cache\" />\n";
    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />\n";
    echo "<meta http-equiv=\"content-language\" content=\"" . $lang['lang'] . "\" />\n";
    echo "<meta http-equiv=\"content-style-type\" content=\"text/css\" />\n";

    echo "<link rel=\"stylesheet\" href=\"../styles/guestbook.css\" type=\"text/css\" />\n";

    // You can include here the CSS of your main-page

    echo "<title>". $lang['guest_login_title'] . "</title>\n";

	echo "</head>\n";
	echo "<body>\n";
	echo "<div id=\"main\">\n";

    // Main-File
    echo "<div id=\"admin\">\n";
    echo "<h1>" . $lang['head_link_guestbook'] . "</h1>\n";

    // If not logged in, show form
    if (isset($_POST['login'])  == false &&
        isset($_POST['delete']) == false &&
        isset($_POST['show'])   == false)
    {
		echo "<p>\n" . $lang['guest_first_login'] . "</p>\n";

		echo "<form action=\"new.php?id=" . $_GET['id'] . "\" method=\"post\">\n";
        echo "<fieldset>\n";
        echo "<legend>" . $lang['guest_login'] . "</legend>\n";
        echo "<label for=\"username\">" . $lang['guest_user_name'] . ": </label>";
        echo "<input name=\"username\" size=\"30\" id=\"username\"/><br />\n";
        echo "<label for=\"password\">" . $lang['guest_password'] . ": </label>";
        echo "<input name=\"password\" type=\"password\" size=\"30\" id=\"password\"/><br />\n";
        echo "<input type=\"submit\" value=\"" . $lang['guest_login'] . "\" name=\"login\"/>\n";
        echo "<input type=\"reset\"  value=\"" . $lang['guest_clear'] . "\"/>\n";
        echo "</fieldset>\n";
        echo "</form>\n";
	}

    // If login ok, ask what to do with the entry
	if (isset($_POST['login']) == true)
	{
		include "../library/password.inc.php";

		$db   = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
		$user = mysql_real_escape_string($_POST['username']);
		mysql_select_db($guest_database);
		$res  = mysql_query("select * from $guest_usertable where username='$user'");
		$db_user     = mysql_result($res, 0, "username");
		$db_password = mysql_result($res, 0, "userpass");
		$db_level    = mysql_result($res, 0, "userlevel");

		if ($user == $db_user && password_check($_POST['password'], $db_password))
		{
			// Write to the session
			$_SESSION['new_loggedin'] = true;
			$_SESSION['ip']           = $_SERVER['REMOTE_ADDR'];
		}
		else
		{
			echo "<p>" . $lang['guest_login_err'] . "</p>\n";
			return;
		}

		// Show the Message according to the id
		$db    = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
		$id    = mysql_real_escape_string($_GET['id']);
		mysql_select_db($guest_database);
		$res   = mysql_query("select * from $guest_table where id = '$id'");
		$num   = mysql_num_rows($res);

		if ($num == 0)
		{
			echo "<p>" . $lang['guest_no_entry'] . "</p>\n";
			return;
		}

		$name    = mysql_result($res, 0, "name");
		$mail    = mysql_result($res, 0, "mail");
		$message = mysql_result($res, 0, "message");
		$time    = mysql_result($res, 0, "time");
		$ip      = mysql_result($res, 0, "ip");
		$state   = mysql_result($res, 0, "state");

		echo "<p>\n";
		echo $lang['guest_mail_from'] . ": " . $name . "<br />\n";
		echo $lang['guest_mail_mail'] . ": " . $mail . "<br />\n";
		echo $lang['guest_mail_message'] . ": <br />\n";
		$message    = utf8_encode($message);
		$message_br = nl2br($message);
		echo $message_br . "<br />\n";
		echo $lang['guest_mail_date'] . ": " . $time . "<br />\n";
		echo $lang['guest_mail_ip'] . ": " . $ip . "\n";
		echo "</p>\n";
		mysql_close($db);

        echo "<form action=\"new.php?id=" . $_GET['id'] . "\" method=\"post\">\n";
        echo "<fieldset>\n";
        echo "<legend>" . $lang['guest_delte_entry'] . "</legend>\n";
        echo "<input type=\"submit\" value=\"";
        echo $lang['guest_delte_entry'] . "\" name=\"delete\"/>\n";
        echo "</fieldset>\n";
        echo "</form>\n";

        echo "<form action=\"new.php?id=" . $_GET['id'] . "\" method=\"post\">\n";
        echo "<fieldset>\n";
        echo "<legend>" . $lang['guest_show'] . "</legend>\n";
        echo "<input type=\"submit\" value=\"";
        echo $lang['guest_show'] . "\" name=\"show\"/>\n";
        echo "</fieldset>\n";
        echo "</form>\n";
	}

	if (isset($_SESSION['new_loggedin']))
	{
		if ($_SESSION['ip'] != $_SERVER['REMOTE_ADDR'])
			return;

		// Delete the new entry
		if (isset($_POST['delete']) == true)
		{
			$db     = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
			mysql_select_db($guest_database);
			$id     = mysql_real_escape_string($_GET['id']);
			$delete = "delete from $guest_table where id = $id";
			mysql_query($delete);
			$num    = mysql_affected_rows();
			echo "<p>";
			if ($num > 0) echo $lang['guest_entry_deleted'];
			else          echo $lang['guest_entry_deleted_err'];
			echo "</p>\n";
			mysql_close($db);
			unset($_SESSION['new_loggedin']);
			session_destroy();
		}

		// Show the new entry
		if (isset($_POST['show']) == true)
		{
			$db     = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
			mysql_select_db($guest_database);
			$id     = mysql_real_escape_string($_GET['id']);
			$update = "update $guest_table set state = '1' where id = $id";
			mysql_query($update);
			$num    = mysql_affected_rows();
			echo "<p>";
			if ($num > 0) echo $lang['guest_show_entry'];
			else          echo $lang['guest_show_entry_err'];
			echo "</p>\n";
			mysql_close($db);
			unset($_SESSION['new_loggedin']);
			session_destroy();
		}
	}

    echo "</div>\n";

    // Footer
    // If you don't need the footer at the bottom of the page, you
    // only have to comment out the next line.
    include "../footer.html";

	echo "</div>\n";
	echo "</body>\n";
	echo "</html>\n";
?>