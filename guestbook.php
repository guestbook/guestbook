﻿<?php
    /**
      @file   guestbook.php
      @author Ruhland Matthias

      @brief  The main-functions of the guestbook
    */

    /**
      @brief Shows additional info in the HTML-Header
     */
    function showHead() {
        include 'include_lang.php';

        echo '<title>' , $lang['guest_title'] , '</title>' . "\n";
        echo '<meta name="description" content="' , $lang['guest_content'] , '" />' , "\n";
        echo '<meta name="keywords" content="' , $lang['guest_keywords'] , '" />' , "\n";
    }

    /**
      @brief Shows the page
     */
    function showPage() {
        include 'include_lang.php';
        include 'settings/settings.php';

        $_SESSION['page'] = 'guestbook';

        echo '<div id="guest">' , "\n";

        echo '<h1>' , $lang['guestbook_title'] , '</h1>' , "\n";

        // A new entry was made
        if (isset($_POST['sent']) === true) {
            if ($_POST['name'] === '' || $_POST['message'] === '')
                echo $lang['guest_fill_all'];
            else {
                $db          = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
                $sendstring  = 'insert into ' , $guest_table , ' ';
                $sendstring .= '(name, mail, message, time, ip, state) values (';
                $name        = mysql_real_escape_string($_POST['name']);
                $sendstring .= "'$name'";
                $email       = mysql_real_escape_string($_POST['mail']);
                $sendstring .= ", '$email'";
                $message     = mysql_real_escape_string($_POST['message']);
                $sendstring .= ", '$message'";
                $date        = getdate();
                $time        = $date['mday'] , '.' , $date['mon'] , '.' , $date['year'] , ' ' , $date['hours'] , ':' , $date['minutes'];
                $sendstring .= ", '$time'";
                $ip          = $_SERVER['REMOTE_ADDR'];
                $sendstring .= ", '$ip'";
                // Show or hide the entry. The admin has to release it.
                if ($guest_set_check == "yes") $sendstring .= ", '0'";
                else                           $sendstring .= ", '1'";
                $sendstring .= ')';
                
                
                mysql_select_db($guest_database);
                mysql_query($sendstring);
                $num = mysql_affected_rows();
                
                
                $new_id = mysql_insert_id();
                mysql_close($db);
                echo '        <p/>' , "\n";
                if ($num >= 1)
                    echo $lang['guest_message_sent'] , '<br />' , "\n";
                else {
                    echo $lang['guest_message_send_er'] , '<br />' , "\n";
                    return;
                }

                // If the Admin want to check the new entry, send him a e-mail
                if ($guest_set_check == "yes") {
                    //! @todo Is there a new Version of the PHPMailer available?
                    include 'class.phpmailer.php';
                    $mail = new PHPMailer();
                    $mail->IsSMTP();
                    $mail->Host     = $guest_smtp_host;
                    $mail->SMTPAuth = $guest_smtp_auth;
                    $mail->Username = $guest_smtp_user;
                    $mail->Password = $guest_smtp_pass;
                    $mail->SetFrom($guest_smtp_sender, $guest_smtp_sendername);
                    //$mail->From     = $guest_smtp_sender;
                    //$mail->FromName = $guest_smtp_sendername;
//                    $mail->CharSet  = "iso-8859-1";
                    $mail->CharSet  = 'utf-8';
                    $mail->IsHTML(true);
                    $mail->AddAddress($guest_smtp_receiver);
                    $mail->Subject  = $lang['guest_mail_subject'];
                    $mail_body  = $lang['guest_mail_body'] , '<br />' , "\n";
                    $mail_body .= $lang['guest_mail_from'];
                    $mail_body .= ': ' , $name , '<br />' , "\n";
                    $mail_body .= $lang['guest_mail_mail'];
                    $mail_body .= ': ' , $email , '<br />' , "\n";
                    $mail_body .= $lang['guest_mail_date'];
                    $mail_body .= ': ' , $time , '<br />' , "\n";
                    $mail_body .= $lang['guest_mail_ip'];
                    $mail_body .= ': ' , $ip , '<br />' , "\n";
                    $mail_body .= $lang['guest_mail_message'];
                    $message_br = nl2br($message);
                    $mail_body .= '<br /> ' , $message_br ' , <br />' , "\n";
                    //$mail_body .= 'http://' , $guest_path , '/admin/new.php?id=' , $new_id;
                    //$mail_body .= '<a href="jaruhl.redirectme.net/test/guestbook/admin/neu.php?id="' , $new_id , "\"> " , $lang['guest_mail_link']  , "</a>\n";
                    $mail_body .= '<a href="' , $guest_path , '/admin/neu.php?id=' , $new_id , '"> ' , $lang['guest_mail_link']  , '</a>';
                    $mail->Body = $mail_body;
                    $mail->MsgHTML($mail_body);
                    if($mail->Send())
                        echo $lang['guest_mail_ok'] , '<br />' , "\n";
                    else
                        echo $lang['guest_mail_error'] , '<br />' , "\n";

                }
            }
        }

        // Pagination
        // Count the entries
        
        
        $db      = mysql_connect($guest_db_host, $guest_db_user, $guest_db_passwd);
        mysql_select_db($guest_database);
        $res     = mysql_query("select count(*) as counter from $guest_table where state = '1'");
        $counter = mysql_fetch_array($res);
        
        
        $counter = $counter['counter'];
        // Calculate the number of pages
        $page_count = ceil($counter / $guest_entr_page);

        $page = (isset($_GET['pageno'] === true))?$_GET['pageno']:0;
        //if (isset($_GET['pageno']))
        //    $page = $_GET['pageno'];
        //else
        //    $page = 0;

        // If the users wants a higher page than available, give him page 0
        if ($page > $page_count)
            $page = 0;

        $start = $page * $guest_entr_page;

        // Read only entries with the state 1 (show)
        
        
        mysql_select_db($guest_database);
        $res = mysql_query("select * from $guest_table where state ='1' order by time desc LIMIT $start, $guest_entr_page");
        $num = mysql_num_rows($res);
        
        

        // Print the entries
        for ($i = 0; $i < $num; $i++) {
            $name    = mysql_result($res, $i, 'name');
            $message = mysql_result($res, $i, 'message');
            $time    = mysql_result($res, $i, 'time');
            $state   = mysql_result($res, $i, 'state');

            if ($state == "1") {
                echo '<p>' , "\n";
                echo '<strong class="line-up">' , "\n";
                echo '<strong class="name">' , $name</strong> , "\n";
                echo '<strong class="time">' , $time</strong> , "\n";
                echo '</strong>' , "\n";
                echo '<br/>' , "\n";
                echo '<strong class="message">' , "\n";
                $message    = utf8_encode($message);
                $message_br = nl2br($message);
                echo $message_br , "\n";
                echo '</strong>' , "\n";
                echo '</p>' , "\n";
            }
        }

        mysql_close($db);

        // Don't show the formular for a new entry, if the user just entered one.
        if (isset($_POST['sent']) === false)
        {
            echo '<form action="index.php?page=guestbook" method="post">' , "\n";
            echo '<fieldset>' , "\n";
            echo '<legend>' , $lang['guest_legend'] , '</legend>' , "\n";
            echo '<label for="name" class="gb">' , $lang['guest_name'] , ': </label>';
            echo '<input name="name" size="20" id="name" /> ' , $lang['guest_name_data'] , '<br />' , "\n";
            echo '<label for="mail" class="gb">' , $lang['guest_mail'] , ': </label>';
            echo '<input name="mail" size="20" id="mail" maxlength="30" /> ' , $lang['guest_mail_data'] , '<br />' , "\n";
            echo '<label for="message">' , $lang['guest_message'] , ': </label>' , "\n";
            echo '<textarea cols="50" rows="4" name="message" id="message"></textarea><br />' , "\n";
            echo '<input type="submit" value="' , $lang['guest_send'] , '" name="sent"/>' , "\n";
            echo '<input type="reset"  value="' , $lang['guest_clear'] , '"/>' , "\n";
            echo '</fieldset>' , "\n";
            echo '</form>' , "\n";
        }

        // If there are more than one page, show the page-selection.
        if ($page_count >= 2) {
            echo $lang['guestbook_page'] , ': ';
            $server = $_SERVER['PHP_SELF'];
            for ($i = 0; $i < $page_count; $i++)
                echo '<a href="' , $server , '?pageno=' , $i , '"> ' , ($i + 1) , ' </a>' , "\n";
        }

        echo '    </div>' , "\n";
    }
?>