﻿<?php
    /**
      @file   include_lang.php
      @author Ruhland Matthias

      @brief  Includes the file with the language

      If there is no language stored, use the default (german, de).
      If there is a other language stored in the session, check if
      the file exists and load it.
    */

    include 'library/check_lang.php';       // Checks the Language the user want's

    // Check stored language
    if (isset($_SESSION['lang']) === true)
    {
        if (is_file('lang/guestbook_' , $_SESSION['lang'] , '.php') === true)
            include 'lang/guestbook_' , $_SESSION['lang'] , '.php';
        
        //if (file_exists("lang/guestbook_".$_SESSION['lang'] . ".php"))
        //    include "lang/guestbook_" . $_SESSION['lang'] . ".php";
    }
    else
        include 'lang/guestbook_de.php';    // The default language
?>