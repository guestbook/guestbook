<?php
    /**
      @file   include_page.php
      @author Ruhland Matthias

      @brief  Include the page the user want to see

      The default page is 'guestbook'.
      For other index.php-files with other default-pages,
      you need other include_page.php-files.
     */

    include 'links.php';

    if (isset($_GET['page']) === false) {               // Visitor won't a special page (fist visit, change lang)
        if (isset($_SESSION['page']) === false)         // No site stored in the session (first visit)
            $page = $links['guestbook'];                // Index-page
        else
        {
            // If visitors site is registered, load it else load the index-page
            $page = (isset($links[$_SESSION['page']]) === true)?$links[$_SESSION['page']]:$links['guestbook'];
            //if (isset($links[$_SESSION['page']]))    
            //    $page = $links[$_SESSION['page']];
            //else                                    // Else load the index-page
            //    $page = $links['guestbook'];
        }
    }
    else
    {
        // If visitors site is registered, load it else load the index-page
        $page = (isset($links[$_SESSION['page']]) === true)?$links[$_SESSION['page']]:$links['guestbook'];
        //if (isset($links[$_GET['page']]))            // If the page is registered, load it
        //    $page = $links[$_GET['page']];
        //else                                        // Else show him the index-page
        //    $page = $links['guestbook'];
    }

    include $page;        // include the page the user want to see
?>