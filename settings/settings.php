﻿<?php
    /**
      @file   settings.php
      @author Ruhland Matthias

      @brief  The settings for the guestbook
    */

    // Database Data
    $guest_db_host           = 'localhost';             //!< Host of the Database for the guestbook
    $guest_db_user           = 'test_guestbook';        //!< User for the database-server
    $guest_db_passwd         = 'guestbook';             //!< Password for the database-server
    $guest_database          = 'test_guestbook';        //!< The Database for the guestbook
    $guest_table             = 'jr_gb_guestbook';       //!< The table for the guestbook
    
    $guest_db_exist          = 'no';                    //!< If a Database exist (some Webspaces9 ("yes"|"no")
    
    $guest_usertable         = 'jr_gb_user';            //!< The table for the users

    $guest_set_check         = 'yes';                   //!< Admin has to check and release new entries ("yes"|"no")
    
    $guest_path              = 'localhost/guestbook';   //!< Path to the guestbook
    $guest_entr_page         = 10;                      //!< Number of entries per page

    // E-Mail Data
    $guest_smtp_host         = 'smtp.web.de';           //!< The SMTP-Host to send the mail
    
    $guest_smtp_auth         = 'true';                  //!< Authentication for SMTP needed ("true"|"false")
    
    $guest_smtp_user         = 'test@web.de';           //!< Username for SMTP-Authentication
    $guest_smtp_pass         = 'test';                  //!< Password for SMTP-Authentication
    $guest_smtp_sender       = 'test@web.de';           //!< Shown sender of the Mail
    $guest_smtp_sendername   = 'JRs Guestbook';         //!< Name of the sender of the Mail
    $guest_smtp_receiver     = 'admin@web.de';          //!< The Receiver of the Mail
?>
